Router.route('/', {
	name: 'home',
	template: 'home'
});
Router.route('/games', {
	name: 'games',
	template: 'gamesPage'
});
Router.route('/preferences', {
	name: 'preferences',
	template: 'preferences'
});
Router.route('/game/:_id', {
	template: 'gamePage',
	data: function () {
		var currentGame = this.params._id;
		Session.set("selectedGame", currentGame);
		return LeagueGames.findOne({
			_id: currentGame
		});
	}
});
Router.route('/user/:_id', {
	template: 'userProfile',
	data: function () {
		var currentUser = this.params._id;
		Session.set("selectedUser", currentUser);
		return Meteor.users.findOne({
			_id: currentUser
		});
	}
});