'use strict';

Template.gamePage.helpers({
	hasPicked: function () {
		var pick = GamePicks.findOne({
			userId: Meteor.userId(),
			gameId: Session.get("selectedGame")
		});
		if (typeof pick !== 'undefined') {
			if (pick.winningPick === null) {
				return false;
			} else {
				return true;
			}
		}
	},
	gameFinished: function () {
		var game = LeagueGames.findOne({
			_id: Session.get("selectedGame")
		});
		if (typeof game !== 'undefined') {
			if (game.isFinished) {
				return true;
			} else {
				return false;
			}
		}
	},
	pickedWinner: function () {
		var game = LeagueGames.findOne({
				_id: Session.get("selectedGame")
			}),
			pick = GamePicks.findOne({
				userId: Meteor.userId(),
				gameId: Session.get("selectedGame")
			});
		if (pick.winningPick === "team1") {
			if (game.scoreTeam1 > game.scoreTeam2) {
				return true;
			} else {
				return false;
			}
		} else if (pick.winningPick === "team2") {
			if (game.scoreTeam2 > game.scoreTeam1) {
				return true;
			} else {
				return false;
			}
		}
	},
	findPick: function () {
		var game = LeagueGames.findOne({
				_id: Session.get("selectedGame")
			}),
			pick = GamePicks.findOne({
				userId: Meteor.userId(),
				gameId: Session.get("selectedGame")
			});
		if (pick.winningPick === "team1") {
			return game.team1;
		} else if (pick.winningPick === "team2") {
			return game.team2;
		} else {
			return null;
		}
	}
});

Template.gamePage.events({
	"submit .pick-winner-form": function (event) {
		// Prevent default browser form submit
		event.preventDefault();

		// Get value from form element
		var pick = event.target.winner.value,
			selectedGame = Session.get("selectedGame"),
			document = GamePicks.findOne({
				userId: Meteor.userId(),
				gameId: selectedGame
			});
		if (typeof document === 'undefined') {
			GamePicks.insert({
				userId: Meteor.userId(),
				gameId: selectedGame,
				winningPick: pick
			});
		} else {
			GamePicks.update(document._id, {
				$set: {
					userId: Meteor.userId(),
					gameId: selectedGame,
					winningPick: pick
				}
			}, {
				upsert: true
			});
		}
	},
	"submit .set-scores-form": function (event) {
		// Prevent default browser form submit
		event.preventDefault();

		// Get value from form element
		var score_1 = event.target.score1.value,
			score_2 = event.target.score2.value,
			selectedGame = Session.get("selectedGame"),
			currentGame = selectedGame,
			currentUser = Meteor.userId(),
			picks = GamePicks.find({
				gameId: selectedGame
			}),
			game,
			playerData,
			pick;

		LeagueGames.update(selectedGame, {
			$set: {
				scoreTeam1: score_1,
				scoreTeam2: score_2,
				isFinished: true
			}
		}, {
			upsert: true
		});
		// Clear form
		event.target.score1.value = "";
		event.target.score2.value = "";

		game = LeagueGames.findOne({
			_id: currentGame
		});
		picks.forEach(function (pick) {
			playerData = PlayerData.findOne({
				userId: pick.userId
			});
			if (pick.winningPick === "team1") {
				if (game.scoreTeam1 > game.scoreTeam2) {
					if (typeof playerData === 'undefined') {
						console.log("team 1 wins: insert");
						PlayerData.insert({
							userId: pick.userId,
							wins: [game._id],
							losses: []
						});
					} else {
						console.log("team 1 wins: update");
						PlayerData.update(playerData._id, {
							$set: {
								userId: pick.userId
							},
							$push: {
								wins: game._id
							}
						});
					}
				} else {
					if (typeof playerData === 'undefined') {
						console.log("team 1 loses: insert");
						PlayerData.insert({
							userId: pick.userId,
							losses: [game._id],
							wins: []
						});
					} else {
						console.log("team 1 loses: update");
						PlayerData.update(playerData._id, {
							$set: {
								userId: pick.userId
							},
							$push: {
								losses: game._id
							}
						});
					}
				}
			} else if (pick.winningPick === "team2") {
				if (game.scoreTeam2 > game.scoreTeam1) {
					if (typeof playerData === 'undefined') {
						console.log("team 2 wins: insert");
						PlayerData.insert({
							userId: pick.userId,
							wins: [game._id],
							losses: []
						});
					} else {
						console.log("team 2 wins: update");
						PlayerData.update(playerData._id, {
							$set: {
								userId: pick.userId
							},
							$push: {
								wins: game._id
							}
						});
					}
				} else {
					if (typeof playerData === 'undefined') {
						console.log("team 2 loses: insert");
						PlayerData.insert({
							userId: pick.userId,
							losses: [game._id],
							wins: []
						});
					} else {
						console.log("team 2 loses: update");
						PlayerData.update(playerData._id, {
							$set: {
								userId: pick.userId
							},
							$push: {
								losses: game._id
							}
						});
					}
				}
			}
		});

	}
});