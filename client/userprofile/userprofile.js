'use strict';

Template.userProfile.helpers({
	userId: function () {
		return Session.get("selectedUser");
	},
	favoriteTeam: function () {
		var selectedUser = Session.get("selectedUser"),
			preferences = AccountPreferences.findOne({
				_id: selectedUser
			});

		return preferences.favoriteTeam;
	},
	wins: function () {
		var selectedUser = Session.get("selectedUser"),
			playerData = PlayerData.findOne({
				userId: selectedUser
			});

		return playerData.wins;
	},
	winRate: function () {
		var selectedUser = Session.get("selectedUser"),
			playerData = PlayerData.findOne({
				userId: selectedUser
			});

		return ((playerData.wins / (playerData.losses + playerData.wins)) * 100).toPrecision(4);
	}
});