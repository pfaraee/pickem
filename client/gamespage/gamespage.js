'use strict';

Template.gamesPage.helpers({
	// returns all league games and sorts them by week
	findGames: function () {
		return LeagueGames.find({}, {
			sort: {
				gameWeek: 1
			}
		});
	},
	weeks: [
		{
			week: "1"
		},
		{
			week: "2"
		},
		{
			week: "3"
		},
		{
			week: "4"
		},
		{
			week: "5"
		},
		{
			week: "6"
		},
		{
			week: "7"
		},
		{
			week: "8"
		},
		{
			week: "9"
		},
		{
			week: "10"
		},
		{
			week: "11"
		},
		{
			week: "12"
		},
		{
			week: "13"
		},
		{
			week: "14"
		},
		{
			week: "15"
		},
		{
			week: "16"
		},
		{
			week: "17"
		}
	],
	teams: [
		{
			teamName: "49ers"
		},
		{
			teamName: "Bears"
		},
		{
			teamName: "Bengels"
		},
		{
			teamName: "Bills"
		},
		{
			teamName: "Broncos"
		},
		{
			teamName: "Browns"
		},
		{
			teamName: "Buccaneers"
		},
		{
			teamName: "Cardinals"
		},
		{
			teamName: "Chargers"
		},
		{
			teamName: "Cheifs"
		},
		{
			teamName: "Colts"
		},
		{
			teamName: "Cowboys"
		},
		{
			teamName: "Dolphins"
		},
		{
			teamName: "Eagles"
		},
		{
			teamName: "Falcons"
		},
		{
			teamName: "Giants"
		},
		{
			teamName: "Jaguar"
		},
		{
			teamName: "Jets"
		},
		{
			teamName: "Lions"
		},
		{
			teamName: "Packers"
		},
		{
			teamName: "Panthers"
		},
		{
			teamName: "Patriots"
		},
		{
			teamName: "Raiders"
		},
		{
			teamName: "Rams"
		},
		{
			teamName: "Ravens"
		},
		{
			teamName: "Redskins"
		},
		{
			teamName: "Saints"
		},
		{
			teamName: "Seahawks"
		},
		{
			teamName: "Steelers"
		},
		{
			teamName: "Texans"
		},
		{
			teamName: "Titans"
		},
		{
			teamName: "Vikings"
		}
	]
});

Template.gamesPage.events({
	// adds new games to the season
	"submit .set-game-form": function (event) {
		// prevents default browser submit
		event.preventDefault();

		var team_1 = event.target.team_1.value,
			team_2 = event.target.team_2.value,
			week = event.target.week.value;

		LeagueGames.insert({
			team1: team_1,
			team2: team_2,
			gameWeek: week,
			isFinished: false
		});

		// clears form
		event.target.team_1.value = "";
		event.target.team_2.value = "";
		event.target.week.value = "";
	},
	"submit .end-week": function (event) {
		event.preventDefault();
		console.log("week over");
	}
});