'use strict';

Template.preferences.helpers({
	favoriteTeam: function () {
		var preferences = AccountPreferences.findOne({
			_id: Meteor.userId()
		});
		return preferences.favoriteTeam;
	},
	teams: [
		{
			teamName: "49ers"
		},
		{
			teamName: "Bears"
		},
		{
			teamName: "Bengels"
		},
		{
			teamName: "Bills"
		},
		{
			teamName: "Broncos"
		},
		{
			teamName: "Browns"
		},
		{
			teamName: "Buccaneers"
		},
		{
			teamName: "Cardinals"
		},
		{
			teamName: "Chargers"
		},
		{
			teamName: "Cheifs"
		},
		{
			teamName: "Colts"
		},
		{
			teamName: "Cowboys"
		},
		{
			teamName: "Dolphins"
		},
		{
			teamName: "Eagles"
		},
		{
			teamName: "Falcons"
		},
		{
			teamName: "Giants"
		},
		{
			teamName: "Jaguar"
		},
		{
			teamName: "Jets"
		},
		{
			teamName: "Lions"
		},
		{
			teamName: "Packers"
		},
		{
			teamName: "Panthers"
		},
		{
			teamName: "Patriots"
		},
		{
			teamName: "Raiders"
		},
		{
			teamName: "Rams"
		},
		{
			teamName: "Ravens"
		},
		{
			teamName: "Redskins"
		},
		{
			teamName: "Saints"
		},
		{
			teamName: "Seahawks"
		},
		{
			teamName: "Steelers"
		},
		{
			teamName: "Texans"
		},
		{
			teamName: "Titans"
		},
		{
			teamName: "Vikings"
		}
	]
});

Template.preferences.events({
	// sets and updates the players favorite football team
	"submit .favorite-team-form": function (event) {
		// prevents default browser form submit
		event.preventDefault();

		// Get value from form element
		var team = event.target.team.value;
		AccountPreferences.update({
			_id: Meteor.userId()
		}._id, {
			$set: {
				favoriteTeam: team
			}
		}, {
			upsert: true
		});
	}
});