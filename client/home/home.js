'use strict';

Template.home.helpers({
	players: function () {
		return PlayerData.find({});
	},
	yourId: function () {
		return Meteor.userId();
	},
	users: function () {
		return Meteor.users.find({});
	}
});
Template.player.helpers({
	winPercentage: function (user) {
		var selectedUser = user,
			playerData = PlayerData.findOne({
				userId: selectedUser
			});
		if (playerData.wins.length === 0) {
			return 0;
		} else if (playerData.losses.length === 0) {
			return 100;
		} else {
			return ((playerData.wins.length / (playerData.losses.length + playerData.wins.length)) * 100).toPrecision(4);
		}
	},
	winRate: function (user) {
		var selectedUser = user,
			playerData = PlayerData.findOne({
				userId: selectedUser
			});

		return playerData.wins.length;
	},
	loseRate: function (user) {
		var selectedUser = user,
			playerData = PlayerData.findOne({
				userId: selectedUser
			});

		return playerData.losses.length;
	}

});