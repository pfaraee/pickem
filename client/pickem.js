"use strict";

Template.registerHelper("isAdmin", function () {
	if (Meteor.user()) {
		if (Meteor.user().emails[0].address === 'admin@admin.com') {
			return true;
		} else {
			return false;
		}
	}
});
Template.registerHelper("isLoggedIn", function () {
	if (Meteor.userId()) {
		return true;
	} else {
		return false;
	}
});